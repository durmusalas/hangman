#include <stdio.h>
#include <string.h>

#define WIDTH 10
#define HEIGHT 8

char out[HEIGHT][WIDTH];

// schreibt String horizontal an x und y Koordinate
void print2Dh(int x, int y, char *str) {
    for(int i = x; *str != '\0'; i++, str++) {
        
        out[y][i] = *str;
    }
}

// schreibt String vertikal an x und y Koordinate
void print2Dv(int x, int y, char *str) {
    for(int i = y; *str != '\0'; i++, str++) {
        out[i][x] = *str;
    }
}

void printHangman(int n) {

    // clear out
    for(int i = 0; i < HEIGHT; i++) {
        for(int j = 0; j < WIDTH - 1; j++) {
            out[i][j] = ' ';
        }
        out[i][WIDTH - 1] = '\0';
    }

    if(n > 0) print2Dh(0,7,"_____");
    if(n > 1) print2Dv(2,1,"|||||||");
    if(n > 2) print2Dh(2,0,"______");
    if(n > 3) out[1][7] = '|';
    if(n > 4) out[2][7] = 'O';
    if(n > 5) print2Dv(7,3,"||");
    if(n > 6) out[5][6] = '/';
    if(n > 7) out[5][8] = '\\';
    if(n > 8) out[3][6] = '\\';
    if(n > 9) out[3][8] = '/';


    // print all lines
    for(int i = 0; i < HEIGHT; i++) {
       printf("%s\n", out[i]);
    }
}

// int main() {

//     for(int i = 0; i < 11; i++) {
//         printHangman(i);
//     }

    
//     return 0;
// }
